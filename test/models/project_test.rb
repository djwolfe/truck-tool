require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  test "can build child items" do
    project = Project.new
    pi = project.project_items.build
    pi.note = "A Note"
    assert true, pi.note == "A Note" 
  end
end

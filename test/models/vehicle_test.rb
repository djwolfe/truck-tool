require 'test_helper'

class VehicleTest < ActiveSupport::TestCase
  test "m3 calculation" do
     veh = vehicles(:vehicle1cbm)
     assert_equal(1.000, veh.m3)
  end
  
  test "place item on vehicle" do
	veh = vehicles(:vehicle1cbm)
	item = items(:item1cbm)
	veh.place(item, 0, 0)
	assert_equal 1, veh.items.size
  end
  
  test "place item over edge of vehicle" do
	veh = vehicles(:vehicle1cbm)
	item = items(:item1cbm)
	veh.place(item, 0.6, 0.6)
	assert_equal 0, veh.items.size
  end
  
  test "place item over edge of vehicle (rounding)" do
	veh = vehicles(:vehicle1cbm)
	item = items(:item1cbm)
	veh.place(item, 0.5, 0.5)
	assert_equal 0, veh.items.size
  end
  
  test "place with neg coords" do
    veh = vehicles(:vehicle1cbm)
	item = items(:item1cbm)
	veh.place(item, -1.0, -0.5)
	assert_equal 0, veh.items.size
  end

end

# TRUCK TOOL #

David Wolfe 2014

This is an educational tool allowing you to experiment with placing loads on a truck.
Written in Rails 4

## Running ##

This app can be started in devel mode the usual way:
```
cd /to/dir/truck_tool
rails s
```

## Deployment ##

This app is compatible with deployment to heroku and requires no special steps outside the normal deployment procedure.
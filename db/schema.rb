# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140713053705) do

  create_table "items", force: true do |t|
    t.string   "desc"
    t.decimal  "l"
    t.decimal  "w"
    t.decimal  "h"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "kgs",        default: 0, null: false
  end

  create_table "project_items", force: true do |t|
    t.integer  "project_id"
    t.integer  "item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "note"
    t.integer  "x"
    t.integer  "y"
    t.boolean  "rotate",     default: false, null: false
  end

  create_table "project_vehicles", force: true do |t|
    t.integer  "project_id"
    t.integer  "vehicle_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "x"
    t.integer  "y"
  end

  add_index "project_vehicles", ["project_id"], name: "index_project_vehicles_on_project_id"
  add_index "project_vehicles", ["vehicle_id"], name: "index_project_vehicles_on_vehicle_id"

  create_table "projects", force: true do |t|
    t.string   "title"
    t.string   "owner"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vehicle_items", force: true do |t|
    t.integer  "vehicle_id"
    t.integer  "item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "vehicle_items", ["item_id"], name: "index_vehicle_items_on_item_id"
  add_index "vehicle_items", ["vehicle_id"], name: "index_vehicle_items_on_vehicle_id"

  create_table "vehicles", force: true do |t|
    t.string   "desc"
    t.decimal  "l"
    t.decimal  "w"
    t.decimal  "h"
    t.integer  "gvm"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "size_class"
    t.string   "make"
    t.string   "model"
  end

end

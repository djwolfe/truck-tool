
vehicle_list = [
  [1, "Hilux Ute",                       2.4,   1.7,  2,     0, "1 Tonne", "Toyota","Hilux"],
  [2, "Transit 280 SWB Van Low Roof",    2.582, 1.39, 1.43,  0, "VAN", "Ford", "Transit"],
  [3, "Transit 330 MWB Van Mid Roof",    2.949, 1.39, 1.645, 0, "VAN", "Ford", "Transit"],
  [4, "20ft Container",                  5.885, 2.35, 2.403, 0, "20","Generic","20 Foot"],
  [5, "40ft Container",                 12.033, 2.35, 2.394, 0, "40","Generic","20 Foot"],
  [6, "40ft HC Container",              12.024, 2.35, 2.697, 0, "40HC","Generic","40 Foot High Cubic"],
]

vehicle_list.each do | id, desc, l, w, h, gvm, size_class, make, model |
  Vehicle.create(id: id, desc: desc, l: l, w: w, h: h, gvm: gvm, size_class: size_class, make: make, model: model)
end

item_list = [
  [1, "Aus Standard Pallet",   1.165, 1.165, 0.15,   0],
  [2, "US Pallet (40""x48"")", 1.219, 1.016, 0.15,   0],
  [3, "IBC 1000L",             1.0,   1.2,   1.2, 1000],
  [4, "Generic 1CBM",          1.0,   1.0,   1.0,  250],
  [5, "Double Pallet",         2.33,  1.165, 0.15,   0],
]

item_list.each do | id, desc, l, w, h, kgs |
  Item.create(id: id, desc: desc, l: l, w: w, h: h, kgs: kgs)
end

project_list = [
  [1, "Sample 1 (Blank)", "D Wolfe"],
  [2, "Sample 2 (20ft Shipping Container)", "D Wolfe"],
]

project_list.each do | id, title, owner |
  Project.create(id: id, title: title, owner: owner)
end

project_item_list = []

4.times do |i|
  project_item_list << [2, 3, "IBC #{(i+1)}", 50, 50 + (i * 80)]
end

project_item_list.each do | project_id, item_id, note, x, y |
  ProjectItem.create(project_id: project_id, item_id: item_id, note: note, x: x, y: y)
end

ProjectVehicle.create(project_id: 2, vehicle_id: 4, x:40, y: 40);
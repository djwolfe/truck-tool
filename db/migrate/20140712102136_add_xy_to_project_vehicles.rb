class AddXyToProjectVehicles < ActiveRecord::Migration
  def change
    add_column :project_vehicles, :x, :integer
    add_column :project_vehicles, :y, :integer
  end
end

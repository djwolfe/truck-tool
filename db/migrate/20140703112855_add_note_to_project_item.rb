class AddNoteToProjectItem < ActiveRecord::Migration
  def change
    add_column :project_items, :note, :string
  end
end

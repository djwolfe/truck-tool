class AddXyToProjectItems < ActiveRecord::Migration
  def change
    add_column :project_items, :x, :integer
    add_column :project_items, :y, :integer
  end
end

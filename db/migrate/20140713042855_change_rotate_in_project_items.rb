class ChangeRotateInProjectItems < ActiveRecord::Migration
  def change
    change_column :project_items, :rotate, :boolean, :default=> false, :null => true
  end
end

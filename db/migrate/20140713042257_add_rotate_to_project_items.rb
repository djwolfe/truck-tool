class AddRotateToProjectItems < ActiveRecord::Migration
  def change
    add_column :project_items, :rotate, :boolean
  end
end

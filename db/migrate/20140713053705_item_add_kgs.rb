class ItemAddKgs < ActiveRecord::Migration
  def change
    add_column :items, :kgs, :integer, after: :h, null: false, default: 0
  end
end

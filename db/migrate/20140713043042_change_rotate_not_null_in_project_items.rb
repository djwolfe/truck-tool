class ChangeRotateNotNullInProjectItems < ActiveRecord::Migration
  def change
    change_column :project_items, :rotate, :boolean, :default=> false, :null => false 
  end
end

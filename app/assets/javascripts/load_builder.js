
$(function(){

  // Vehicle and Item settings
  draggable_opts = {
    stop: function() {
      save_position(this);
    },
  };

  $( '.draggable-icon' ).draggable({
    helper: "clone"
  });

  $( '.item, .vehicle' ).draggable(draggable_opts);

  $('.vehicle').click(function() {
    update_selection(this); 
  });

  $('.item').click(function() {
    update_selection(this); 
  });

  $('#tool-remove').click(function() { alert('Drag items here to delete') });
  
  $( "#tool-remove" ).droppable({
    hoverClass: "tool-hover",
    tolerance: "touch",
    drop: function( event, ui ) {
      var parts = ui.draggable.attr('id').split("_");
      var type = parts[0];
      var id = parts[1];
      $(ui.draggable).remove();
      $.ajax({
        url: project_id() + "/delete_element",
        type: "GET",
        data: { id: id, type: type },
        error: ajax_error
      });
    }
  });
  
  $( "#workspace" ).droppable({
    accept: ".draggable-icon",
    hoverClass: "ui-state-hover",
    drop: function( event, ui ) {
	
      if($(ui.draggable).hasClass("vehicle-icon")) {
        var newDiv = new_element('vehicle');
        newDiv.appendTo( this );
        vehicle_dropped(this, event, ui, newDiv);
      } else if($(ui.draggable).hasClass("item-icon")) {
        var newDiv = new_element('item');
        newDiv.appendTo( this );
        item_dropped(this, event, ui, newDiv);
      }
    }
  });

  $( '#tool-rotate' ).click(function () {
    rotate($('.selected-element'));
  });
});

// create vehicle or item div
function new_element(type) {

  var div = $( '<div class="'+type+'"></div>' );

  div.draggable(draggable_opts);

  div.click(function() {
    update_selection(this); 
  });
 
  return div;
}

function rotate(element) {
  var cur_width  = element.width();
  var cur_height = element.height();

  element.width(cur_height);
  element.height(cur_width);
  
  var parts = $(element).attr('id').split("_");
  var type = parts[0];
  var id = parts[1];

  $.ajax({
    url: project_id() + "/rotate",
    type: "GET",
    data: {
      id: id,
      type: type
    },
    error: ajax_error
  });
}

// m to pixels ratio
function scale() {
  return $("#scale").val();
}

function update_selection(element) {
  display_info(element);
  $('.vehicle').removeClass('selected-element');
  $('.item').removeClass('selected-element');
  $(element).addClass('selected-element');
}

function vehicle_dropped(workspace, event, ui, vehicleDiv) {
  
  vehicleDiv.click(function() {
    display_info(this);
  });
  
  var parts = ui.draggable.attr('id').split("_");
  var vehicle_id = parts[1];
        
  $.ajax({ 
    url: project_id() + "/add_vehicle",
    type: "GET",
    data: { vehicle_id: vehicle_id},
    success: function(data) {
      new_vehicle_added(data, vehicleDiv);
    },
    error: ajax_error
  });
}

function ajax_error(data) {
  alert(data.status + " " + data.statusText);
  console.log(data);
}

function item_dropped(workspace, event, ui, itemDiv) {

  itemDiv.click(function() {
    display_info(this);
  });

  parts = ui.draggable.attr('id').split("_");
  item_id = parts[1];
        
  $.ajax({ 
    url: "add_item",
    type: "GET",
    data: { 
      item_id: item_id,
      project_id: project_id()
    },
    success: function(data) {
      new_item_added(data, itemDiv);
    },
    error: ajax_error
  });
}

function save_position(element) {

  var parts = $(element).attr('id').split("_");
  var type = parts[0];
  var id = parts[1];

  var pos = $(element).position();

  $.ajax({
    url: project_id() + "/save_position",
    type: "GET",
    data: {
      id: id,
      type: type,
      top: pos.top,
      left: pos.left
    },
    error: ajax_error
  });
}

function project_id() {
  return $("#project_id").val();
}

function new_vehicle_added(data, div) {
  div.outerWidth(data.vehicle.w * scale());
  div.outerHeight(data.vehicle.l * scale());
  div.attr("id", "project-vehicle_" + data.project_vehicle.id);
  div.text(data.vehicle.desc);
}

function new_item_added(data, div) {
  div.outerWidth(data.item.w * scale());
  div.outerHeight(data.item.l * scale());
  div.attr("id", "project-item_" + data.project_item.id);
  div.text(data.item.desc);
}

function display_info(item) {
  var pos = $(item).position();
  $('#tool-info').text($(item).text() + " @ " + pos.left + ',' + pos.top);
}

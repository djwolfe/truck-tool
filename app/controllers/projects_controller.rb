class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy, :manifest]
  layout 'application', :except => [:manifest]
	  
  def index
    @projects = Project.all
  end

  def show
  end

  def new
    @project = Project.new
    @project.project_items.build 
    @project.project_vehicles.build 
  end

  def edit
    @project.project_items.build
    @project.project_vehicles.build 
  end

  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to edit_project_path(@project), notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Basic freight manifest report of the projects load
  def manifest
    render :manifest
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(
        :id, :title, :owner, :project_items, :project_vehicles,
        project_items_attributes: [:id, :note, :item_id, :_destroy],
        project_vehicles_attributes: [:id, :vehicle_id, :_destroy]
      )
    end
end

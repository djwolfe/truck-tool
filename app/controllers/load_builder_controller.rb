class LoadBuilderController < ApplicationController

  # render the initial html view of the page
  def index
    @vehicles = Vehicle.all
    @items = Item.all
    @scale = 70.0;  # m to pixel
    @project = Project.find(lb_params[:project_id])
  end
  
  def add_vehicle
    @project = Project.find(lb_params[:project_id])
    v = Vehicle.find(lb_params[:vehicle_id])
    pv = @project.project_vehicles.build
    pv.vehicle = v
    @project.project_vehicles << pv
    render json: {project_vehicle: pv, vehicle: v}
  end
  
  def add_item
    @project = Project.find(lb_params[:project_id])
    it = Item.find(lb_params[:item_id])
    pi = @project.project_items.build    
    pi.item = it
    @project.project_items << pi
    render json: {project_item: pi, item: it}
  end
 
  def save_position 
    e = find_element()
    if e != nil
      e.x = lb_params[:left]
      e.y = lb_params[:top]
      e.save
    end
    render json: e
  end

  def delete_element 
    e = find_element()
    e.destroy
    render json: true
  end

  def rotate
    e = find_element()
    e.rotate = !e.rotate
    render json: e.save
  end  

  # vehicle or item
  def find_element
    @project = Project.find(lb_params[:project_id])
    if lb_params[:type] == 'project-item'
      @project.project_items.find_by_id(lb_params[:id])
    elsif lb_params[:type] == 'project-vehicle'
      @project.project_vehicles.find_by_id(lb_params[:id])
    end
  end
 
  private
    def lb_params
      params.permit(:vehicle_id, :item_id, :project_id, :top, :left, :type, :id)
    end

end

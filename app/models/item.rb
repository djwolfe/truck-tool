# Item of freight common definition
class Item < ActiveRecord::Base
  #has_one :project, through: :project_items
  validates :desc, presence: true,
                   length: { minimum: 3 }

  def m3
    l * w * h
  end
end

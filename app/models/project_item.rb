# An item of freight that is part of a project
# including placement and rotation
class ProjectItem < ActiveRecord::Base
  belongs_to :project
  belongs_to :item

  def w
    rotate ? item.l : item.w
  end

  def l
    rotate ? item.w : item.l
  end

end

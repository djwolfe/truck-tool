class ProjectVehicle < ActiveRecord::Base
  belongs_to :project
  belongs_to :vehicle
end

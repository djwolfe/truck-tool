class VehicleItem < ActiveRecord::Base
  belongs_to :vehicle
  belongs_to :item
end


# Vehicle (or loadable container) that can accept items of freight
# l,w,h are the dimensions of the loadable area of the vehicle
# x,y are coordinates with origin (0,0) top left of a north facing vehicle

class Vehicle < ActiveRecord::Base
  has_many :project_vehicles
  has_many :projects, through: :project_vehicles
  has_many :vehicle_items
  has_many :items, through: :vehicle_items
  
  def m3
	l * w * h
  end
  
  # attempt to place an item on this vehicle at x,y
  def place(item, x, y)
    if in_bounds(x, y) && fits_in(item, x, y)
      items << item
    end  
  end
  
  def fits_in(item, x, y)  
    x + item.w <= self.w && y + item.l <= self.l
  end
  
  def in_bounds(x, y)
    x >= 0.0 && x < self.w && y >= 0.0 && y < self.l
  end
end

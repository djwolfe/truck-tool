class Project < ActiveRecord::Base
  has_many :project_vehicles, dependent: :destroy
  has_many :vehicles, through: :project_vehicles
  has_many :project_items, dependent: :destroy
  has_many :items, through: :project_items

  accepts_nested_attributes_for :project_items,
                                {:reject_if => lambda { |a| a[:item_id].blank? }, 
                                :allow_destroy => true }

  accepts_nested_attributes_for :project_vehicles,
                                { :reject_if => lambda { |a| a[:vehicle_id].blank? },
                                :allow_destroy => true }
end

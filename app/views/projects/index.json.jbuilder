json.array!(@projects) do |project|
  json.extract! project, :id, :title, :owner
  json.url project_url(project, format: :json)
end

json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :desc, :class, :l, :w, :h, :gvm
  json.url vehicle_url(vehicle, format: :json)
end

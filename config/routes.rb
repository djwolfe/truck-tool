Rails.application.routes.draw do
  
  resources :vehicle_types do
    member do
      get 'duplicate_existing'
    end
  end

  resources :projects

  get 'welcome/index'
  get 'load_builder/:project_id/add_vehicle', to: 'load_builder#add_vehicle'
  get 'load_builder/add_item', to: 'load_builder#add_item'
  get 'load_builder/:project_id', to: 'load_builder#index'
  get 'load_builder/:project_id/save_position',  to: 'load_builder#save_position'
  get 'load_builder/:project_id/delete_element', to: 'load_builder#delete_element'
  get 'load_builder/:project_id/rotate', to: 'load_builder#rotate'

  get 'projects/:id/manifest', to: 'projects#manifest'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  resources :items
  resources :vehicles

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
